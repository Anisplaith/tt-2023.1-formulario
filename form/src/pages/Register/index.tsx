import React from "react";
import { CircleAnimation } from "../../components/CircleAnimation";
import { RegisterCard } from "../../components/RegisterCard";
import { RegisterCard2 } from "../../components/RegisterCard2";
import { PageRegister } from "./styles";

export function Register() {
    return (
        <PageRegister>
            <CircleAnimation></CircleAnimation>
            <RegisterCard></RegisterCard> 
        </PageRegister>
    )
}