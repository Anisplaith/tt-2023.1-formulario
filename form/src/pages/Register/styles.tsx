import styled from "styled-components";

export const PageRegister = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 20px;
    width: 100vw;
    height: 100vh;
    background-color: #0A2647;
`