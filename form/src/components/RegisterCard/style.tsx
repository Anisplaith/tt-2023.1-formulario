import styled from "styled-components";

export const FormRegisterCard = styled.form`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    gap: 45px;
    width: 30vw; // 30% da largura viewport [tela do navegador sem scroll (rolagem)]
    height: 70vh; // 75% da altura viewport [tela do navegador sem scroll (rolagem)]
    background-color: #2e6ec2;
    border-radius: 20px;
    z-index: 10;
    opacity: 0.8;

    @media(min-width: 1920px){ // media query
        height: 56vh;
    }
`

export const InputRegister = styled.input`
    width: 98%;
    height: 40px;
    background-color: #0A2647;
    // google fonts
    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@500&display=swap');    font-family: 'Inter', sans-serif;
    color: #f2f2f2;
    border-radius: 15px;
    padding-left: 10px;
    border: none;
    outline: none;
    &:focus{
        outline: none;
    }
`
export const InputContainer = styled.div`
    width: 90%;
    height: 35px;
    border-radius: 15px;
`
export const Label = styled.p`
    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@500&display=swap');    font-family: 'Inter', sans-serif;
    color: #f2f2f2;
    padding-bottom: 5px;
`
export const Title = styled.h1`
    font-family: 'Inter', sans-serif;
    color: #f2f2f2;
`

