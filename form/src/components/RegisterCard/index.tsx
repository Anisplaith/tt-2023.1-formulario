import React, { useState } from 'react'
import { RegisterButton } from '../RegisterButton'
import { FormRegisterCard, InputContainer, InputRegister, Label, Title } from './style'

import { useForm } from "react-hook-form";
import InputMask  from 'react-input-mask/index';

export function RegisterCard() {
    const [email, setEmail] = useState<string>('')
    const [CPF, setCPF] = useState<string>('')
    const [telephone, setTelephone] = useState<string>('')

    const { register, formState: { errors }, handleSubmit } = useForm();

    const data = {
        email,
        CPF,
        telephone
    }
    
    const onSubmit = () => {
        alert(JSON.stringify(data))
        setEmail('')
        setCPF('')
        setTelephone('')
    }

    // style do InputMask
    const InputStyle = {
        width: '98%',
        height: '40px',
        backgroundColor: '#0A2647',
        color: '#f2f2f2',
        borderRadius: '15px',
        paddingLeft: '10px',
        border: 'none',
        outline: 'none'
    }

    return (
        <FormRegisterCard onSubmit={handleSubmit(onSubmit)}>
            <Title>Cadastre-se</Title>
            <InputContainer>
                <Label>E-mail</Label>
                <InputRegister
                    {...register('email', {required: true})}
                    placeholder='exemplo@.com'
                    onChange={(event:any) => {setEmail(event.target.value); console.log(`Email: ${email}`)}}
                    value = {email}
                >
                </InputRegister>
                {errors.email?.type === 'required' &&
                <p style={{color: '#FF9F9F', fontSize: '13px'}}>Campo obrigatório</p>}
            </InputContainer>

            <InputContainer>
                <Label>CPF</Label>
                <InputMask 
                    {...register('cpf', {required: true, minLength: 14})}
                    mask = {'999.999.999-99'}
                    maskChar = {''}
                    placeholder='999.999.999-99'
                    onChange={(event:any) => {setCPF(event.target.value); console.log(`CPF: ${CPF}`)}}
                    value = {CPF}
                    style = {InputStyle}
                >
                </InputMask>

                {errors.cpf?.type === 'required' &&
                <p style={{color: '#FF9F9F', fontSize: '13px'}}>Campo obrigatório</p>}
                {errors.cpf?.type === 'minLength' &&
                <p style={{color: '#FF9F9F', fontSize: '13px'}}>Formato Inválido</p>}
            </InputContainer>

            <InputContainer>
                <Label>Telefone</Label>
                <InputMask 
                    {...register('tel', {required: true, minLength: 14})}
                    mask = {'(99) 9999-9999'}
                    maskChar = {''}
                    placeholder='(99) 9999-9999'
                    onChange={(event:any) => {setTelephone(event.target.value); console.log(`Telefone: ${telephone}`)}}
                    value = {telephone}
                    style = {InputStyle}
                >
                </InputMask>

                {errors.tel?.type === 'required' &&
                <p style={{color: '#FF9F9F', fontSize: '13px'}}>Campo obrigatório</p>}
                {errors.tel?.type === 'minLength' &&
                <p style={{color: '#FF9F9F', fontSize: '13px'}}>Formato inválido</p>}
            </InputContainer>
            
            <RegisterButton></RegisterButton>

        </FormRegisterCard>
    )
}