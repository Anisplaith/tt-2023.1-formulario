import React from 'react'
import { Circles, CirclesContainer } from './style'

// ajuda o map a mostrar os círculos na tela e determina o delay
var delayCount = ['1.2s', '0.2s', '2s', '0.8s', '1.6s', '2.3s','1.8s']
export function CircleAnimation (){
    return(
        <CirclesContainer>
            {
                delayCount.map((item, index)=>(
                    <Circles key={index} order={item}></Circles>
                ))
            }
        </CirclesContainer>
    )
}