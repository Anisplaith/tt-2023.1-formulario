import styled from "styled-components";
interface Order {
    order: string
}

export const CirclesContainer = styled.div`
    display: flex;
    justify-content: center;
    gap: 135px;
    width: 97%;
    height: fit-content;
`

export const Circles = styled.div<Order>`
    display: block;
    width: 40px;
    height: 40px;
    border-radius: 100px;
    background-color: #acd4f7;
    opacity: 0.7;
    transform: translateY(-20vh);
    // produz a animação de acordo com a % do tempo estipulado. Ex: 70% de 5s
    @keyframes CircleAnimation { 
        0%{
            transform: translateY(-20vh) scale(0.6);
        }
        70%{
            transform: translateY(90vh) scale(0);
        }
        100%{
            transform: translateX(8vw) scale(1);
        }
    }
    animation: CircleAnimation 5s infinite linear reverse;
    animation-delay: ${prop=> prop.order};
`