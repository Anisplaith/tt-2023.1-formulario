import React from "react";
import { RegisterButton } from "../RegisterButton";
import { FormRegisterCard, InputContainer, Label, InputRegister, Title } from "../RegisterCard/style";

// É hora de praticar! Se quiser monte a validação e as máscaras seguindo a aula de Forms aqui!

export function RegisterCard2 (){
    return(
        <FormRegisterCard>
            <Title>Cadastre-se</Title>
            <InputContainer>
                <Label>E-mail</Label>
                <InputRegister
                    placeholder='exemplo@.com'
                >
                </InputRegister>
            </InputContainer>

            <InputContainer>
                <Label>CPF</Label>
                <InputRegister 
                    placeholder='999.999.999-99'
                >
                </InputRegister>
            </InputContainer>

            <InputContainer>
                <Label>Telefone</Label>
                <InputRegister 
                    placeholder='(99) 9999-9999'
                >
                </InputRegister>
            </InputContainer>
            
            <RegisterButton></RegisterButton>

        </FormRegisterCard>
    )
}