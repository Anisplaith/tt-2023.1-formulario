import styled from 'styled-components'


export const Button = styled.button`
    width: 35%;
    height: 8%;
    margin-top: 30px;
    border-radius: 5px;
    border: none;
    background-color: #f2f2f2;
    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@500&display=swap');    font-family: 'Inter', sans-serif;
    color: #0A2647;
    font-weight: 600;
    font-size: 17px;
    &:hover{
        background-color: #0A2647;
        color: #f2f2f2;
        cursor: pointer;
        transition-delay: 0.2s;
    }
`