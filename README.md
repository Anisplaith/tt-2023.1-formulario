# TT 2023.1 - Formulário

## Para rodar o projeto:

```
 1) vá para a pasta 'form' no terminal
 2) dê yarn install
 3) para rodar projeto no navegador: dê yarn start
```

## Comandos e importações vistas na aula

```

# OBS: Se vc já deu **yarn install** nesse projeto, então NÃO precisa dar nenhum comando abaixo.
React JS
---------------

FORM:
 
. Comando: yarn add react-hook-form

. Importação: import { useForm } from "react-hook-form";

. Dentro da função principal: 

const { register, formState: { errors }, handleSubmit } = useForm();


MASK:

. Comandos:

1) yarn add @types/react-input-mask
2) yarn add react-input-mask

. Importação: import InputMask  from 'react-input-mask/index' ou só tirar o index da importação


React Native
--------------------------
FORM:

. Comando: yarn add react-hook-form

. Importação: import { useForm, Controller } from "react-hook-form"

. Dentro da função principal: 

const { handleSubmit, control, formState: { errors } } = useForm({});


MASK:

. Comando: yarn add react-native-masked-text

. Importação: import { TextInputMask } from 'react-native-masked-text';

```


## Project status

Finished
